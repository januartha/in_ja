const gulp = require("gulp");
const sass = require("gulp-sass");

const webpack = require("webpack");
const webpackConfig = require("./webpack.config");
const webpackDevMiddleware = require("webpack-dev-middleware");

const BrowserSync = require('browser-sync');
const browser = BrowserSync.create()
const bundler = webpack(webpackConfig);

const fileinclude = require("gulp-file-include");

const jsFiles = "src/js/**/*.js";
const scssFiles = "src/css/**/*.scss";
const htmlFiles = "src/*.html";
const imgFiles = "src/images/**/*.+(jpg|jpeg|png|gif|svg)";
const dist = "dist/"

gulp.task("server", (callback) => {
    let browserConfig = {
        server: dist,
        open: false,
        middleware: [
            webpackDevMiddleware(bundler, {})
        ]
    };
    browser.init(browserConfig);

    gulp.watch(scssFiles, ["scss"]);
    gulp.watch(jsFiles, ["js"]);
    gulp.watch(htmlFiles, ["html"]);
    gulp.watch(imgFiles, ["images"]);
    gulp.watch(dist + "**/*.*").on("change", () => browser.reload());
})

gulp.task("js", (callback) => {
    webpack(webpackConfig, (error, stats) => {
        if(error) {{ /* do something */ }}
        callback()
    })
})

gulp.task("scss", (callback) => {
    return gulp.src(scssFiles)
            .pipe(sass().on("errr", sass.logError))
            .pipe(gulp.dest(dist + "css/"));
})

gulp.task("html", (callback) => {
    return gulp.src(htmlFiles)
            .pipe(fileinclude({
                prefix: "@@",
                basepath: "src"
            }))
            .pipe(gulp.dest(dist));
})

gulp.task("images", (callback) => {
    return gulp.src(imgFiles)
            .pipe(gulp.dest(dist + "images/"))
})

gulp.task("default", ["js", "html", "scss", "images", "server"]);
gulp.task("build", ["js", "html", "scss", "images"]);