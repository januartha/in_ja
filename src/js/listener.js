export const registerEventListener = (el, eventName, fn) => {
    if(el.addEventListener) {
        el.addEventListener(eventName, fn);
    } else {
        el.attachEvent("on" + eventName, fn);
    }
}
