import stars from "../templates/stars"

const userBio = ((user) => {
    return `<div class="user__avatar" style="background-image: url('images/profile_image.jpg');"></div>
    <div class="user__details">
        <div>
            <h3 class="user__name">${user.firstName} ${user.lastName}</h3>
        </div>
        <div><ion-icon name="call"></ion-icon>${user.phone}</div>
        <div><ion-icon name="pin"></ion-icon>${user.address}</div>
        <ul class="user__stars screen__small">${stars(user.stars, "icon__star")}</ul>
        <div class="screen__small"><strong>${user.reviews}</strong> reviews</div>
        <div class="screen__small"><ion-icon name="add-circle"></ion-icon><strong>${user.followers}</strong> Followers</div>
    </div>
    <div class="user__stats screen__large">
        <div>
            <ul class="user__stars">${stars(user.stars, "icon__star--float")}</ul>
        </div>
        <div><strong>${user.reviews}</strong> reviews</div>
    </div>
    `
})
export default userBio