const userAboutEdit = ((user) => {
    return `<h2>About</h2>
    <div class="float__label">
        <input type="text" id="first-name" value="${user.firstName}" required>
        <label for="first-name">First Name</label>
    </div>
    <div class="float__label">
        <input type="text" id="last-name" value="${user.lastName}" required>
        <label for="last-name">Last Name</label>
    </div>
    <div class="float__label">
        <input type="text" id="website" value="${user.website}" required>
        <label for="website">Website</label>
    </div>
    <div class="float__label">
        <input type="text" id="phone" value="${user.phone}" required>
        <label for="phone">Phone</label>
    </div>
    <div class="float__label">
        <input type="text" id="address" value="${user.address}" required>
        <label for="address">City, State & Zip</label>
    </div>
    <div class="button__group--float">
        <button onClick="cancelEdit()">Cancel</button>
        <button onClick="saveBio()">Save</button>
    </div>
    `
})
export default userAboutEdit