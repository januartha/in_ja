const dialogBox = (el, value) => {
    //console.log(getBindNames(el))
    let prev = document.getElementById("dialog")
    if(prev) prev.parentNode.removeChild(prev)
    let rec = el.getBoundingClientRect()
    let dialog = document.createElement("div")
    dialog.id = "dialog"
    dialog.innerHTML = template(value)
    dialog.style.left = rec.x + rec.width
    el.parentNode.appendChild(dialog)
}

const getBindNames = (el) => {
    let nodeBind = null
    if(el.parentNode.hasAttribute("bind")) {
        nodeBind = el.parentNode
    } else {
        nodeBind = el.parentNode.querySelector("[bind]")
    }
    if(nodeBind) return nodeBind.getAttribute("bind").split(" ").join("").split(",")
    return null
}

const template = (value) => {
    return `${value.map((item, index) => {
        return `<div class="float__label">
            <input type="text" id="label${index}" value="${item.value}" required>
            <label for="label${index}">${item.label}</label>
        </div>`
    })}        
    <button class="button__outline" onClick="cancelEdit()">Cancel</button>
    <button class="button__filled" onClick="saveBio()">Save</button>
    `
}

export default dialogBox