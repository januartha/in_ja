import stars from "../templates/stars"

const userAbout = ((user) => {
    return `<h2>About</h2>
    <div>
        <div class="page__textfield page__textfield--edit">
            <h3 bind="firstName, lastName">${user.firstName} ${user.lastName}</h3>
            <button class="button--edit" onClick="editItem(this, [{label:'First Name', value:'${user.firstName}'}, {label:'Last Name', value:'${user.lastName}'}])"><ion-icon name="create" class="screen__large"></ion-icon></button>
        </div>
    </div>
    <div>
        <div class="page__textfield page__textfield--edit" bind="website">
            <ion-icon name="md-globe"></ion-icon> ${user.website}
            <button class="button--edit" onClick="editItem(this, [{label: 'Website', value: '${user.website}'}])"><ion-icon name="create" class="screen__large"></ion-icon></button>
        </div>
    </div>
    <div>
        <div class="page__textfield page__textfield--edit" bind="phone">
            <ion-icon name="call"></ion-icon> ${user.phone}
            <button class="button--edit" onClick="editItem(this, [{label: 'Phone', value: '${user.phone}'}])"><ion-icon name="create" class="screen__large"></ion-icon></button>
        </div>
    </div>
    <div>
        <div class="page__textfield page__textfield--edit" bind="address">
            <ion-icon name="home"></ion-icon> ${user.address}
            <button class="button--edit" onClick="editItem(this, [{label: 'City, State & ZIP', value: '${user.address}'}])"><ion-icon name="create" class="screen__large"></ion-icon></button>
        </div>
    </div>
    <div>
        <button class="button--edit float__top__right screen__small" onClick="editBio()"><ion-icon name="create"></ion-icon></button>
    </div>
    `
})
export default userAbout