const renderStars = (stars => {
    let res = []
    let m = 0
    while(m < Math.floor(stars)){
        res.push("star")
        m++
    }
    if(stars % 1 > 0) res.push("star-half")
    let s = 5 - res.length
    m = 0
    if(s > 0) {
        while(m < Math.floor(s)){
            res.push("star-outline")
            m++
        }
    }
    return res
})

const stars = ((data, className) => {
    return `<ul>
        ${renderStars(data).map(star => `<li><ion-icon name="${star}" class="${className}"></ion-icon></li>`).join("")}
    </ul>
    `
})

export default stars