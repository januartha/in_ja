import userBio from "./templates/user-bio"
import userAbout from "./templates/user-about"
import userAboutEdit from "./templates/user-about-edit"
import stars from "./templates/stars"
import dialogBox from "./templates/dialog-box"

import "./menu";

//
let bio = {
    firstName: "Jessica",
    lastName: "Parker", 
    phone: "(949) 325-68594", 
    address: "Newport Beach, CA",
    website: "www.seller.com",
    stars: 4.5,
    reviews: 10,
    followers: "150K"
}

const init = () => {
    dataBinding()
    //document.querySelector(".user__bio").innerHTML = userBio(bio)
    document.querySelector("#about").innerHTML = userAbout(bio)
    //document.querySelector(".menu .user__followers").innerHTML = `<ion-icon name="add-circle"></ion-icon><strong>${bio.followers}</strong> Followers`
}

window.editBio  = () => {
    document.querySelector("#about").innerHTML = userAboutEdit(bio)
}
window.saveBio = () => {
    bio.firstName = document.getElementById("first-name").value
    bio.lastName = document.getElementById("last-name").value
    bio.website = document.getElementById("website").value
    bio.phone = document.getElementById("phone").value
    bio.address = document.getElementById("address").value
    init()
}
window.cancelEdit = () => init();
window.editItem = (el, value) => {
    dialogBox(el, value)
}

//data binding
const dataBinding = () => {
    let bindNames = []
    let bindNamesLength = 0
    let value
    let bindings = Array.from(document.querySelectorAll("[bind]"))
    bindings.map(item => {
        bindNames = item.getAttribute("bind").split(" ").join("").split(",")
        bindNamesLength = bindNames.length
        Array.from(item.childNodes).map(node => {
            if(node.nodeName == "#text") {
                value = ""
                bindNames.map((bindName, index) => {
                    value += bio[bindName]
                    if(index < bindNamesLength) value += " "
                })
                node.textContent = value
            }
        })
    })
}

init()