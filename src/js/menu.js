import {registerEventListener} from "./listener"

const showPage = (btnTarget) => {
    tabButtons.map(btn => {
        if(btn === btnTarget) btnTarget.className += " active";
        else btn.className = btn.className.replace("active", "");
    })
    let label = btnTarget.innerText || btn.textContent;
    pages.map(page => {
        if(document.getElementById(label.toLowerCase()) == page) page.style.display = "block";
        else page.style.display = "none";
    })
}
const tabButtons = [].slice.call(document.querySelectorAll(".tab__button"));
const pages = [].slice.call(document.querySelectorAll(".page"));
tabButtons.map(btn => {
    registerEventListener(btn, "click", () => {
        showPage(btn)
    })
})
// init
tabButtons[0].click();